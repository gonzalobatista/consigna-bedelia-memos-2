require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'rspec/expectations'
require 'json'
require 'byebug'
require_relative '../../app'

include Rack::Test::Methods

def app
  Sinatra::Application
end

After do | escenario |
  post '/reset', 'CONTENT_TYPE' => 'application/json'
end
